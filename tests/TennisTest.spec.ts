import { Tennis } from "./Tennis"

describe("Tennis Kata", () => {
  it("when party has not started score is Love-All", () => {
    const tennis = new Tennis()

    expect(tennis.getScore()).toEqual("Love-All")
  })

  it("should score Fifteen-Love", () => {
    const tennis = new Tennis()

    tennis.playerOneScored()

    expect(tennis.getScore()).toEqual("Fifteen-Love")
  })
  it("should score Thirty-Love", () => {
    const tennis = new Tennis()

    tennis.playerOneScored()
    tennis.playerOneScored()

    expect(tennis.getScore()).toEqual("Thirty-Love")
  })

  it("should score Forty-Love", () => {
    const tennis = new Tennis()

    tennis.playerOneScored()
    tennis.playerOneScored()
    tennis.playerOneScored()

    expect(tennis.getScore()).toEqual("Forty-Love")
  })

  it("should score Win for player 1", () => {
    const tennis = new Tennis()

    tennis.playerOneScored()
    tennis.playerOneScored()
    tennis.playerOneScored()
    tennis.playerOneScored()

    expect(tennis.getScore()).toEqual("Win for player1")
  })

  it("should score Love-Fifteen", () => {
    const tennis = new Tennis()

    tennis.playerTwoScored()

    expect(tennis.getScore()).toEqual("Love-Fifteen")
  })

  it("should score Love-Thirty", () => {
    const tennis = new Tennis()

    tennis.playerTwoScored()
    tennis.playerTwoScored()

    expect(tennis.getScore()).toEqual("Love-Thirty")
  })

  it("should score Love-Forty", () => {
    const tennis = new Tennis()

    tennis.playerTwoScored()
    tennis.playerTwoScored()
    tennis.playerTwoScored()

    expect(tennis.getScore()).toEqual("Love-Forty")
  })
})
