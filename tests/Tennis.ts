export class Tennis {
  private playerOneScore: number = 0
  private playerTwoScore: number = 0

  playerOneScored(): void {
    this.playerOneScore++
  }

  playerTwoScored(): void {
    this.playerTwoScore++
  }

  getScore(): string {
    if (this.playerTwoScore === 1) return "Love-Fifteen"
    if (this.playerTwoScore === 2) return "Love-Thirty"
    if (this.playerTwoScore === 3) return "Love-Forty"

    if (this.playerOneScore === 1) return "Fifteen-Love"
    if (this.playerOneScore === 2) return "Thirty-Love"
    if (this.playerOneScore === 3) return "Forty-Love"
    if (this.playerOneScore === 4) return "Win for player1"
    return "Love-All"
  }
}
